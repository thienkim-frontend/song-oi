# Source code for Bootstrap website's template

#### Folder structure

```
source/
----- views/   
---------- index.pug
---------- layout/
---------- includes/
---------- mixin/

----- sass/   
---------- common/
---------- layout/
---------- _home.scss
---------- _responsive.scss
---------- style.scss

----- images/     
---------- common/

----- fonts/      
----- css/        // All styles of jQuery plugins
----- js/         // Third-party libraries such as jQuery, Moment, Underscore, etc.

document/					// Includes all Photoshop, pdf, doc files

screenshot.jpg    // The current recommended size for the screenshot is 1200 x 900 pixels
```
#### Installation

```
$ npm install
$ gulp
$ gulp build --env production
$ gulp iconfont
$ gulp sprite
$ gulp useref

http://localhost:3200
http://192.168.2.137:3200/your_website/public/
```
#### jQuery plugins

  - [ReactJs](https://reactjs.org/)
  - [Underscore](http://underscorejs.org/#debounce)
  - [React lazyload](https://github.com/jasonslyvia/react-lazyload)
  - [Axios for HTTP calls](https://www.npmjs.com/package/axios)
  - [Webpack](https://www.npmjs.com/package/webpack)

#### Features
  - Gulp 
  - Foundation 
  - Sass 
  - Support IE9+

#### Useful information
  - Font: 
  - Responsive: 
  - Document: 
  - Reference/ Current website:
  - Demo/ Preview link:
  - Ftp:
  - Git:

#### Screen-shots
![React Redux Cart Screenshot](https://github.com/navjotdhanawat/react-redux-express-cart/blob/master/client/images/react-redux-cart-1.png?raw=true "React Redux Cart Screenshot")

#### Demo and Tutorial:

[THENEXTFACT Tutorial: How to create shopping cart in react redux](http://www.thenextfact.com/?p=588&preview=true)